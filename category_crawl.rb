require 'rubygems'
require 'rails'
require 'nokogiri'
require 'mechanize'
require 'open-uri'
require 'active_record'
require 'ancestry'
load 'error_logger.rb'

@page = ""

# Database connectivity, keyed from 2nd passed argument.
if ARGV[1] == "production"
	ActiveRecord::Base.establish_connection(
		:adapter => "mysql2",
		:encoding => "utf8",
		:database => "kindletrend_production",
		:host => "localhost",
		:pool => 25,
		:username => "",
		:password => ""
	)
else
	ActiveRecord::Base.establish_connection(
		:adapter => "mysql2",
		:encoding => "utf8",
        :database => "kindletrend_development",
        :username => "kindletrend_user",
        :password => "kindletrend_pwd",
	)
end

######################################
# We use 2 models to collect the data.
#
# Catgory group simply tells us which crawl this data belongs to.
# Category houses the actual crawl data.
######################################
class CategoryGroup < ActiveRecord::Base
	has_many :categories
end

class Category < ActiveRecord::Base
	has_ancestry
	belongs_to :category_group
end

def crawl(id)
	node = Category.find(id)

	if node.has_children?
		node = node.children.first
	else
		node = Category.find(siblings(node.id))
	end
	
	parse_categories(node.id)
end
# End crawl.

# Checks if the current category has siblings, and either routes the crawl if so, or ends it if the crawl has finished.
def siblings(id)
	unless id == nil
		node = Category.find(id)
		# group_root_id replaced with the parent category id.
		
		if node.id == @category_id.to_i
			cat_group = CategoryGroup.find(node.category_group_id)
			cat_group.complete = true
			cat_group.save

			abort "Congratulations! You have a map of all Kindle categories!"
			# exec "open Notification.app"
		elsif node.has_siblings?
			sibs = node.sibling_ids.sort
			if sibs[sibs.index(node.id).to_i] < sibs.last
				sibs[sibs.index(node.id).to_i + 1]
			else
				unless node.parent_id == @category_id.to_i
					siblings(node.parent_id)
				else
					abort "Congratulations! You have a map of all Kindle categories!"
				end
			end
		else
			unless node.parent_id == @category_id.to_i
				siblings(node.parent_id)
			else
				abort "Congratulations! You have a map of all Kindle categories!"
			end
		end
	else
		puts "No category_id. All titles mapped?"
	end
end

# Book data is only scraped for certain categories. If the category that is currently being crawled is flagged for watching
# in the previous crawl (category group), flag here it here as well.
def update_watches
	last_watch = Category.where("watch IS NOT NULL").last
	count = Category.where("category_group_id = #{cat_group.id} AND watch > 0").count

	unless count > 0
		prior_watches = Category.where("category_group_id = #{cat_group.id} AND watch > 0")
		current_root = Category.where(category_group_id: cat_group.id).first
	end
end

def parse_categories(id)
	parse_single_category(id)
	node = Category.find(id)
	
    # Amazon has said they will not stop us from what we are doing, but they have also not given us any
    # special access to data. We need to randomize the frequency of 
	sleep(rand(20..30).seconds)

	puts "==============================================================="
	puts ""
	crawl(node.id)
end

def parse_single_category(id)
	node = Category.find(id)
	puts "Current node: #{node.id}"

	# Get the root page.
	rand_browser = 1 + rand(7)
	case(rand_browser)
		when 1 then agent_alias = 'Mac Firefox'
		when 2 then agent_alias = 'Mac Mozilla'
		when 3 then agent_alias = 'Windows Chrome'
		when 4 then agent_alias = 'Windows IE 8'
		when 5 then agent_alias = 'Windows IE 9'
		when 6 then agent_alias = 'Windows Mozilla'
		when 7 then agent_alias = 'Linux Firefox'
	end

	user_agent = Mechanize.new {|agent| agent.user_agent_alias = "#{agent_alias}"}

	user_agent.get(node.url) { |d| @page = d.content }

	@page = Nokogiri::HTML(@page)

	## Get the categories navigation section of the page.
    nav = @page.xpath('//ul[@data-typeid="n"]')

    puts "Nav. count: #{@page.xpath(@xpath_var).children.count}"

	# Get the category names and links for this page.
	if nav.children.count > 0
		puts "Node has children..."
		nav.children.each do |n|
			if n.xpath('a/span[@class="refinementLink"]').to_s.strip != ""
				name = n.xpath('a/span[@class="refinementLink"]').text
				puts "Name: #{name}"
				# 3 lines to distill this is really ugly!
				puts "Nav link = " << n.xpath('a/span[@class="refinementLink"]/../@href').to_s[0,21]
				unless n.xpath('a/span[@class="refinementLink"]/../@href').to_s[0,21] == "http://www.amazon.com"
					url = "http://www.amazon.com" + n.xpath('a/span[@class="refinementLink"]/../@href').to_s
				else
					url = n.xpath('a/span[@class="refinementLink"]/../@href').to_s
				end
					
                asin = url.split("&bbn")[0].split("Cn%3A").pop

				count = n.xpath('a/span[@class="narrowValue"]').text.to_s
				count = count[2, count.length - 3].to_s.delete(",").to_i
				puts "Count: #{count}"

				if name.strip != ""
					item = Category.new(:parent_id => node.id)	
						item.name = "#{name}"
						item.url = "#{url}"
						item.asin = "#{asin}"
						item.count = count
						item.category_group_id = node.category_group_id
					item.save
				end
			end
		end
	end
end

# Begin crawling a specific category.
def new
	parse_categories(@category_id)
end

# In the event the crawl stops or fails mid-way, run will resume at the last crawled record.
def run
	node = Category.last
	
    parse_categories(node.id)
end

# Initialize a new category group by crawling the top-level set of categories.
def init
	cat_group = CategoryGroup.new
	cat_group.save

	category = cat_group.categories.new
		category.name = "Kindle eBooks"
		category.url = "http://www.amazon.com/s/ref=lp_154607011_ex_n_1?rh=n%3A133140011%2Cn%3A!133141011%2Cn%3A154606011&bbn=154606011&ie=UTF8&qid=1392049593"
		category.asin = "154606011"
		category.count = 2482522
	category.save

	cat_group.group_root_id = category.id
	cat_group.save

	parse_single_category(category.id)
end

# ARGV[0] = init/run/new
#    init - Crawl the top-level categories.
#    run - In the event the crawl stops or fails mid-way, run will resume at the last crawled record.
#    new - Begin crawling a specific category.
# ARGV[1] = Database environment to use.
# ARGV[2] = The parent category id - required for new and run, to define the category to begin crawling.

if ARGV[0] == nil || ARGV[1] == nil# || ARGV[2] == nil
	puts "Format for script must be: bundle exec ruby category_crawl.rb 'init'|'new'|'run' 'development'|'production' category_id"
	exit
else
	if (ARGV[0] == "new" || ARGV[0] == "run") && ARGV[2] == nil
		puts "Format for script must be: bundle exec ruby category_crawl.rb 'init'|'new'|'run' 'development'|'production' category_id"
		exit	
	else
		@category_id = ARGV[2]
	end
end

case ARGV[0]
	when "init"
		init()
	when "run"
		run()
	when "new"
		new()
end
